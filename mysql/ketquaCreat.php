<?php 

require('connect2.php');


$sql = "SELECT * FROM sinhvien ";
$query = $db->query($sql);
$sinhvien = $query->fetch_all(MYSQLI_ASSOC);

$sql = "SELECT * FROM mon_hoc ";
$query = $db->query($sql);
$mh = $query->fetch_all(MYSQLI_ASSOC);

$error = [];
$isCreated = 0;
 if (isset($_POST["submit"])) {
 	if (!isset($_POST['masv']) || $_POST["masv"] == '' ) {
 		$error[] = "Vui lòng chọn sinh viên";
    }
    elseif (!isset($_POST['mamh']) || $_POST['mamh'] == ''){
    	$error[] = "Vui lòng chọn  môn học";
    }
    elseif (!isset($_POST['ketqua']) || $_POST['ketqua'] == ''){
    	$error[] = "Vui lòng nhập kết quả";
    }
    else{
    	$masv = $_POST['masv'];
    	$mamh = $_POST['mamh'];
    	$kq = $_POST['ketqua'];
    	// nháy kép để nối chuỗi
    	$sql= "SELECT * FROM mon_hoc where mamonhoc = '". $mamh ."' and masv = '". $mamh ."' LIMIT 1 ";
    	$query = $db->query($sql);
    	$result = $query->fetch_assoc();
    	if (!is_null($result)){
    		$error[] = "Sinh viên này đã có điểm";
    		// thêm dữ liệu insert into tênbảng(cột1, cột2, vvv) values('giátrị1', 'giátrị2', vvvv)
    	}else{
    		$sql = "insert into ketqua(diem, masv, mamonhoc) values ('". $kq ."', '". $mamh ."', '". $tenmh ."')";
    		$query = $db->query($sql);
    		if ($query){
    			$isCreated = 1;
    		}
    	}
    }
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>A</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>
<body>
<div class="container">
	<h2 style="text-align: center;">Thông tin khoa</h2>
	<div class="col-md-3" style="float: left;">
		<table class="table">
			<thead>
				<tr>
					<th><a href="sinhvien.php">Sinh viên</a></th>
				</tr>
				<tr>
					<th><a href="monhoc.php">Môn học</a></th>
				</tr>
				<tr>
					<th><a href="ketqua.php">Kết quả</a></th>
				</tr>
				<tr>
					<th><a href="khoa.php">Các khoa</a></th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-6" style="float: left;">	
		<form class="form" method="POST">
			<label class="label">Mã sinh viên</label>
      		<select name="masv" >
      				<option value="">--Chọn--</option>
        				<?php
        				if (!is_null($sinhvien) && count($sinhvien) > 0 ): 
        					foreach ($sinhvien as $item) :
        				 ?>
        				 <option value="<?php echo $item['masv'];?>"> <?php echo $item['masv'];?> - <?php echo $item['hoten'];?> </option>	
     					<?php 
     					endforeach;
     					endif ;
     					?>
			</select><br>
			<label class="label">Tên môn học</label>
			<select name="mamh" >
      				<option value="">--Chọn--</option>
        				<?php
        				if (!is_null($mh) && count($mh) > 0 ): 
        					foreach ($mh as $item) :
        				 ?>
        				 <option value="<?php echo $item['mamonhoc'];?>"><?php echo $item['tenmonhoc'];?> </option>	
     					<?php 
     					endforeach;
     					endif ;
     					?>
			</select><br>
			<label class="label">Kết quả</label>
			<input class="form-control" type="text" name="ketqua" value="<?php if (isset($_POST['ketqua'])) echo $_POST['ketqua']  ?>">
			<button class="btn" type="submit" name="submit">Thêm mới</button>



		<?php if (count($error) > 0) :?>
  		<?php for ($i=0; $i < count($error); $i++) :?>
    	<p class="thongbao" style="color: red;"><?php echo $error[$i]; ?></p>
		<?php endfor ;?>
 		<?php endif ;?>
		</form>
		<?php if ($isCreated == 1) :?>
    	<p class="success" style="color: green;">Thêm môn học thành công</p>
 		<?php endif ;?>
	</div>
	<div class="col-md-3" style="float: left;">
	</div>
	</div>
</div>
</body>
</html>
<!-- ý tưởng kiểm tra trùng lặp trong csdl
	b1xét thông tin kiểm tra(cột cần kiểm tra)
	b2 truy vấn(lấy) 1 bản ghi theo điều kiện
	vd: select * from where makhoa = "dữ liệu người dùng nhập vào"
	nếu trả về null - không trùng - được thêm dữ liệu
	khác null - thông báo bị trùng