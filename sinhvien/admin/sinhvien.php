<?php 
session_start();
if (!isset($_SESSION['user'])) {
    header("Location:login.php");
}

require('connect2.php');

$sql = "SELECT * FROM sinhvien ";
$query = $db->query($sql);

$result = $query->fetch_all(MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
	<title>A</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>
<body>
<div class="container">
    

	<h2 style="text-align: center;">Danh sách các sinh viên</h2>
	<div class="col-md-3" style="float: left;">
		<table class="table">
			<thead>
				<tr>
					<th><a href="sinhvien.php">Sinh viên</a></th>
				</tr>
				<tr>
					<th><a href="monhoc.php">Môn học</a></th>
				</tr>
				<tr>
					<th><a href="ketqua.php">Kết quả</a></th>
				</tr>
				<tr>
					<th><a href="khoa.php">Các khoa</a></th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-6" style="float: left;">	
		<table class="table" style="border: 1px solid; float: left; ">
			<thead>
			<tr>
				<th style="font-size: 10px;">MaSV</th>
				<th style="font-size: 10px;">TenSV</th>
				<th style="font-size: 10px;">ngaysinh</th>
				<th style="font-size: 10px;">gioitinh</th>
				<th style="font-size: 10px;">diachi</th>
				<th style="font-size: 10px;">email</th>
				<th style="font-size: 10px;">makhoa</th>
				<th>tác vụ</th>
			</tr>
			</thead>
			<tbody>
			<?php 
			if (count($result) > 0) :
				foreach ($result as $sv) :
			?>
			<tr>
				<td style="font-size: 10px;"><?php echo $sv['masv'];?></td>
				<td style="font-size: 10px;"><?php echo $sv['hoten'];?></td>
				<td style="font-size: 10px;"><?php echo $sv['ngaysinh'];?></td>
				<td style="font-size: 10px;"><?php echo $sv['gioitinh'];?></td>
				<td style="font-size: 10px;"><?php echo $sv['diachi'];?></td>
				<td style="font-size: 10px;"><?php echo $sv['email'];?></td>
				<td style="font-size: 10px;"><?php echo $sv['makhoa'];?></td>
				<td><a href="sinhvienEdit.php?mk=<?php echo $sv['masv'];?>"> Edit </a></td>	
			</tr>
			<?php
				endforeach;
			endif; 
			?>
			</tbody>
		</table>
	</div>
	<div class="col-md-3" style="float: left;">
			<table class="table">
			<thead>
				<tr>
					<th><a href="sinhvienCreat.php">Thêm mới</a></th>
				</tr>
				<tr>
					<th><a href="#">Sửa</a></th>
				</tr>
				<tr>
					<th><a href="#">Xóa</a></th>
				</tr>
			</thead>
		</table>
	</div>
	</div>
</div>
</body>
</html>


<?php

/**
 * Lấy 1 bản ghi
 */

$sql = "SELECT * FROM sinhvien LIMIT 1";
$query = $db->query($sql);
$result = $query->fetch_assoc();
//print_r($result);

/**
 * Đếm số bản ghi
 */

$sql = "SELECT COUNT(*) FROM sinhvien";
$query = $db->query($sql);
$result = $query->fetch_row();
//echo $result[0];

$db->close();