<?php 
session_start();
if (!isset($_SESSION['user'])) {
    header("Location:login.php");
}

require('connect2.php');


$sql = "SELECT * FROM khoa ";
$query = $db->query($sql);

$result = $query->fetch_all(MYSQLI_ASSOC);

$error = [];
$isCreated = 0;
 if (isset($_POST["submit"])) {
 	if (!isset($_POST['makhoa']) || $_POST["makhoa"] == '' ) {
 		$error[] = "Vui lòng nhập lại mã khoa";
    }
    elseif (!isset($_POST['ten_khoa']) || $_POST['ten_khoa'] == ''){
    	$error[] = "Vui lòng nhập lại tên khoa";
    }
    else{
    	$makhoa = $_POST['makhoa'];
    	$tenkhoa = $_POST['ten_khoa'];
    	// nháy kép để nối chuỗi
    	$sql= "SELECT * FROM khoa where makhoa = '". $makhoa ."' LIMIT 1 ";
    	$query = $db->query($sql);
    	$result = $query->fetch_assoc();
    	if (!is_null($result)){
    		$error[] = "Mã khoa đã tồn tại, vui lòng nhập mã khoa khác";
    		// thêm dữ liệu insert into tênbảng(cột1, cột2, vvv) values('giátrị1', 'giátrị2', vvvv)
    	}else{
    		$sql = "insert into khoa(makhoa, ten_khoa) values ('". $makhoa ."', '". $tenkhoa ."')";
    		$query = $db->query($sql);
    		if ($query){
    			$isCreated = 1;
    		}
    	}
    }
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>A</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>
<body>
<div class="container">
	<h2 style="text-align: center;">Thông tin khoa</h2>
	<div class="col-md-3" style="float: left;">
		<table class="table">
			<thead>
				<tr>
					<th><a href="sinhvien.php">Sinh viên</a></th>
				</tr>
				<tr>
					<th><a href="monhoc.php">Môn học</a></th>
				</tr>
				<tr>
					<th><a href="ketqua.php">Kết quả</a></th>
				</tr>
				<tr>
					<th><a href="khoa.php">Các khoa</a></th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-6" style="float: left;">	
		<form class="form" method="POST">
			<label class="label">Mã khoa</label>
			<input class="form-control" type="text" name="makhoa" value="<?php if (isset($_POST['makhoa'])) echo $_POST['makhoa']  ?>">
			<label class="label">Tên khoa</label>
			<input class="form-control" type="text" name="ten_khoa"><br>
			<button class="btn" type="submit" name="submit">Thêm mới</button>
		<?php if (count($error) > 0) :?>
  		<?php for ($i=0; $i < count($error); $i++) :?>
    	<p class="thongbao" style="color: red;"><?php echo $error[$i]; ?></p>
		<?php endfor ;?>
 		<?php endif ;?>
		</form>
		<?php if ($isCreated == 1) :?>
    	<p class="success" style="color: green;">Thêm khoa thành công</p>
 		<?php endif ;?>
	</div>
	<div class="col-md-3" style="float: left;">
	</div>
	</div>
</div>
</body>
</html>
<!-- ý tưởng kiểm tra trùng lặp trong csdl
	b1xét thông tin kiểm tra(cột cần kiểm tra)
	b2 truy vấn(lấy) 1 bản ghi theo điều kiện
	vd: select * from where makhoa = "dữ liệu người dùng nhập vào"
	nếu trả về null - không trùng - được thêm dữ liệu
	khác null - thông báo bị trùng