<?php 
session_start();
if (!isset($_SESSION['user'])) {
    header("Location:login.php");
}

require('connect2.php');
$sql = "SELECT * FROM khoa ";
$query = $db->query($sql);
$khoa = $query->fetch_all(MYSQLI_ASSOC);
//b1 lấy ds khoa
$mk = $_GET['mk'];
$sql= "SELECT * FROM sinhvien where masv = '". $mk ."' LIMIT 1 ";
    $query = $db->query($sql);
   // $result = $query->fetch_all(MYSQLI_ASSOC);
  $sinhvien = $query->fetch_assoc();
 if (is_null($sinhvien)){
  	header('location : khoa.php');
  }

$error = [];
$isCreated = 0;
 if (isset($_POST["submit"])) {
 	if (!isset($_POST['masv']) || $_POST["masv"] == '' ) {
 		$error[] = "Vui lòng nhập lại mã sinh viên";
    }
    elseif (!isset($_POST['hoten']) || $_POST['hoten'] == ''){
    	$error[] = "Vui lòng nhập lại họ tên";
    }
    elseif (!isset($_POST['ngaysinh']) || $_POST['ngaysinh'] == ''){
    	$error[] = "Vui lòng nhập ngày sinh";
    }
    elseif (!isset($_POST['gioitinh']) || $_POST['gioitinh'] == ''){
    	$error[] = "Vui lòng nhập ngày sinh";
    }
    elseif (!isset($_POST['diachi']) || $_POST['diachi'] == ''){
    $error[] = "Vui lòng nhập địa chỉ";
    }
     // elseif (!isset($_POST['email']) || $_POST['email'] == ''){
    	//$error[] = "Vui lòng nhập ngày sinh";
  //  }
    else{
    	$masv = $_POST['masv'];
    	$hoten = $_POST['hoten'];
      $matkhau = $_POST['password'];
    	$ngaysinh = $_POST['ngaysinh'];
    	$gioitinh = $_POST['gioitinh'];
    	$diachi = $_POST['diachi'];
    	$email = $_POST['email'];
    	$makhoa = $_POST['makhoa'];
    	// nháy kép để nối chuỗin
    		$sql = "UPDATE sinhvien SET masv ='". $masv ."', hoten='". $hoten ."', password ='". $matkhau ."',ngaysinh='". $ngaysinh ."', gioitinh='". $gioitinh ."', diachi='". $diachi ."', email='". $email ."', makhoa='". $makhoa."' WHERE maSV= '". $mk ."'";
    		
    		$query = $db->query($sql);
    		if ($query){
    			$isCreated = 1;
    		}
    	}
    }



?>
<!DOCTYPE html>
<html>
<head>
	<title>A</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>
<body>
<div class="container">
	<h2 style="text-align: center;">Thông tin sinh viên</h2>
	<div class="col-md-3" style="float: left;">
		<table class="table">
			<thead>
				<tr>
					<th><a href="sinhvien.php">Sinh viên</a></th>
				</tr>
				<tr>
					<th><a href="monhoc.php">Môn học</a></th>
				</tr>
				<tr>
					<th><a href="ketqua.php">Kết quả</a></th>
				</tr>
				<tr>
					<th><a href="khoa.php">Các khoa</a></th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-6" style="float: left;">	
		<form class="form" method="POST">
			<label class="label">Mã Sinh Viên</label>
			<input class="form-control" readonly="readonly" type="text" name="masv" value="<?php if (isset($_POST['masv'])) echo $_POST['masv']; else echo $sinhvien['masv']; ?>">
			<label class="label">Họ tên</label>
			<input class="form-control" type="text" name="hoten" value="<?php if (isset($_POST['hoten'])) echo $_POST['hoten']; else echo $sinhvien['hoten']; ?>">
      <label class="label">Mật khẩu</label>
      <input class="form-control" type="text" name="password" value="<?php if (isset($_POST['password'])) echo $_POST['password']; else echo $sinhvien['password'];  ?>">
			<label class="label">Ngày Sinh</label>
			<input class="form-control" type="date" name="ngaysinh" min="1950-01-01" value="<?php if (isset($_POST['ngaysinh'])) echo $_POST['ngaysinh'];  else echo $sinhvien['ngaysinh'];  ?>">
			<br>
			<label class="label">Giới Tính</label>
			<div class="dropdown">
      				<select name="gioitinh" class="dropdown-select">
        				<option value="1" <?php if((isset($_POST['gioitinh']) && $_POST['gioitinh'] == 1) || $sinhvien['gioitinh'] == 1)  echo 'selected="selected"'; ?>>Nam</option>
       					<option value="0" <?php if((isset($_POST['gioitinh']) && $_POST['gioitinh'] == 0)  || $sinhvien['gioitinh'] == 0) echo 'selected="selected"'; ?>>Nữ</option>
       					
     				</select>
			</div>
      <br>
      <label class="label">Trạng thái</label>
      <input name="trangthai" type="radio" value="1" />Hoạt Động
      <input name="trangthai" type="radio" value="0" />Tạm Khóa<br />
			<br>
			<label class="label">Địa chỉ</label>
			<input class="form-control" type="text" name="diachi" value="<?php if (isset($_POST['diachi'])) echo $_POST['diachi']; else echo $sinhvien['diachi'];  ?>">
			<label class="label">Gmail</label>
			<input class="form-control" type="text" name="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']; else echo $sinhvien['email'];  ?>"><br>
			<label class="label">Mã Khoa</label>
			<!---b2 đưa dữ liệu vào selectbox -->
      		<select name="makhoa" >
      				<option value="">--Chọn--</option>
        				<?php
        				if (!is_null($khoa) && count($khoa) > 0 ): 
        					foreach ($khoa as $item) :
        				 ?>
        				 <option value="<?php echo $item['makhoa'];?>"<?php if ((isset($_POST['makhoa']) && $_POST['makhoa'] == $item['makhoa']) || $sinhvien['makhoa'] == $item['makhoa']) echo 'selected="selected"' ;?>>
                  <?php echo $item['ten_khoa'];?>
                   </option>	
     					<?php 
     					endforeach;
     					endif ;
     					?>
			</select><br>
			<button class="btn" type="submit" name="submit">Sửa</button>
		<?php if (count($error) > 0) :?>
  		<?php for ($i=0; $i < count($error); $i++) :?>
    	<p class="thongbao" style="color: red;"><?php echo $error[$i]; ?></p>
		<?php endfor ;?>
 		<?php endif ;?>
		</form>
		<?php if ($isCreated == 1) :?>
    	<p class="success" style="color: green;">Sửa sinh viên thành công</p>
 		<?php endif ;?>
		
	</div>
	<div class="col-md-3" style="float: left;">
	</div>
	</div>
</div>
</body>
</html>
<!-- lấy hêt thằng cha ra(lấy hết các khoa)
